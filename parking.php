<!--
 * Created by PhpStorm.
 * User: John
 * Date: 01/02/2017
 * Time: 09:07
 -->

<!-- Database connection -->
<?php
require_once('include/common.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>TruckSmart - Parking Reservation</title>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script src="js/createComponentsUtility.js"></script>
    <script src="js/mapScript.js"></script>
    <script src="./timeSpentDriving/driverTimeCals.js"></script>
    <script src="./timeSpentDriving/driveTimeScripts.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/tableStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
    </script>

    <!-- Fav Icons for difference platforms -->
    <link rel="apple-touch-icon" sizes="57x57" href="/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo.png">
    <meta name="theme-color" content="#ffffff">

<body>
<div class="container-fluid">
    <div class="row">
        <!-- include menu -->
        <?php
        include 'template/menu.php'
        ?>
        <h4 style="text-align: center">Please select a location to park.</h4>


        <!-- Reference: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_css_modal2 -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" style="color: white">&times;</span>
                    <img class="img-responsive" src="logoText.png" style="padding: 0;" align="centre"
                         alt="TruckSmart - Parking" width="200" height="350">
                </div>

                <!-- Indicate to the user they have selected a place to stay-->
                <div class="modal-body">

                    <p>

                    <div id="name"></div>
                    <div id="add"></div>
                    <div id="phone"></div>

                    </p>

                    <p>Thank you for reserving a parking spot using TruckSmart - Parking</p>

                </div>
                <div class="modal-footer">
                    <h3>ThinkSmart - ParkSmart</h3>
                </div>

            </div>

        </div>

        <!-- Help pop up -->
        <div class="container">
            <i class="fa fa-question-circle fa-2x" data-toggle="help" style="float: right"
               title="Reserve a Parking Location"
               data-content="Select a parking location by clicking on a desired parking location"></i>
        </div>


        <!-- Displaying parking locations in a table based on information being pulled for places api -->
        <div id="tableWrapper" style="width: 100%;"><br>
            <script>
                populateTable();
                addRowHandlers('mytable');

                document.getElementById("name").innerHTML = localStorage.getItem('pkName');
                document.getElementById("add").innerHTML = localStorage.getItem('pkAdd');
                document.getElementById("phone").innerHTML = localStorage.getItem('pkPhone');

            </script>
            <script src="js/helper.js"></script>
        </div>

        <?php
        include 'template/footer.php'
        ?>
    </div>
</div>
</body>

</html>
