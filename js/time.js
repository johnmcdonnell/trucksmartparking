/**
 * Created by John on 17/03/2017.
 */

/**
 * Converts min and max times
 */
function createSelectTime(element, min, max)
{
    var  select = document.getElementById(element);

    for (var i = min; i <= max; i++)
    {
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }
}

/**
 * Converts dropdown time to mins and stores it in localstorage.
 */
function convertSelectionToMins()
{
    var selectedHrs = document.getElementById("selectHrs").value;
    var selectedMins = document.getElementById("selectMins").value;

    var hrsAsMins = parseInt(selectedHrs) * 60;
    var mins = parseInt(selectedMins);
    var driveTime = (hrsAsMins + mins);
    localStorage.setItem("driveTime", driveTime);
}
