/**
 * Created by John on 24/03/2017.
 */
function addOptionsToSelectItem(selectID, min,max, jump)
{

    var select = document.getElementById(selectID);


    for (var i = min; i < max; i+= jump) {
        var option = document.createElement('option');
        option.setAttribute('value', i);
        option.appendChild(document.createTextNode(i));
        select.appendChild(option);
    }
}

function getValueFromHrsSelectItemInMins()
{
    var elementValue = parseInt(document.getElementById('selectHrs').value);

    return elementValue * 60;

}

function getMinsValueFromSelectItem()
{
    return parseInt(document.getElementById('selectMins').value);
}

function getMinsFromHoursAndMinsSelectItems()
{
    var hoursAsMins = getValueFromHrsSelectItemInMins();
    var mins = getMinsValueFromSelectItem();
    return hoursAsMins + mins;
}

function setDrivetimeValueInLocalStorage()
{
    localStorage.setItem("driveTime",parseInt(getMinsFromHoursAndMinsSelectItems()));

}
