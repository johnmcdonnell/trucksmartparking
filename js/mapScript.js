/**
 * Created by John on 24/03/2017.
 */
var minsArray = []; //Stores the number of minutes between each step of the journey
var latLongArray = []; //Stores the lat and log at each step of the journey
var latLong = "";
var map;
var geocoder;
var markers = [];
var infos = [];

function initMap()
{
    // prepare Geocoder
    geocoder = new google.maps.Geocoder();

    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: {lat: 53.8008, lng: 1.5491}
    });
    directionsDisplay.setMap(map);

    document.getElementById('submit').addEventListener('click', function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
        setTimeout(findPlaces,500);

    });

}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var waypts = [];
    var checkboxArray = document.getElementById('waypoints');
    for (var i = 0; i < checkboxArray.length; i++) {
        if (checkboxArray.options[i].selected) {
            waypts.push({
                location: checkboxArray[i].value,
                stopover: true
            });
        }
    }

    directionsService.route({
        origin: document.getElementById('start').value,
        destination: document.getElementById('end').value,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    }, function(response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            var totalDurationTime = 0;
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                    '</b><br>';

                summaryPanel.innerHTML += route.copyrights + '<br><br>';
                summaryPanel.innerHTML += 'Distance. text = ' +route.legs[i].distance.text + '<br>';
                summaryPanel.innerHTML += 'Distance. value = ' +route.legs[i].distance.value + '<br>';
                summaryPanel.innerHTML += 'Duration. text = ' +route.legs[i].duration.text + '<br>';
                summaryPanel.innerHTML += 'Duration. value = ' +route.legs[i].duration.value + '<br>';
                summaryPanel.innerHTML += 'End Address = ' + route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += 'End Location = ' + route.legs[i].end_location + '<br><br>';
                // summaryPanel.innerHTML += 'Steps distance text = ' +  route.legs[i].steps[i].distance.text + '<br><br>';
                // summaryPanel.innerHTML += 'Steps distance value = ' +  route.legs[i].steps[i].distance.value + '<br><br>';

                for (var j = 0; j < route.legs[i].steps.length; j++)
                {

                    minsArray.push(getMinsFromDuration(route.legs[i].steps[j].duration.text,i));


                    //Get the lat and lon cordinates at every step
                    latLong = route.legs[i].steps[j].end_location;
                    latLongArray.push(latLong);

                }



            }
        } else {
            window.alert('Directions request failed due to ' + status);
        }

        var count = 0;

        var driveTime = localStorage.getItem("driveTime");
        var nextMinLat = 0;
        var nextMinLong = 0;

        for (var a = 0; a < minsArray.length ; a++)
        {
            count++;
            if(count == driveTime) {
                document.getElementById('lat').value = getLatOrLongFromString(latLongArray, a, 0);
                document.getElementById('lng').value = getLatOrLongFromString(latLongArray, a, 1);
            }
            if(minsArray[a] > 1)
            {
                var numMins = minsArray[a];//Number of mins between steps
                var nextLat = parseFloat(getLatOrLongFromString(latLongArray,(a + 1),0));//gets the next lat position
                var thisLat = parseFloat(getLatOrLongFromString(latLongArray,a,0));//gets the current lat position
                var diffInLat = Math.abs(nextLat - thisLat);//gets the difference between the this lat position and the next latposition
                var difInLatForEveryMin = parseFloat(diffInLat / minsArray[a]);
                var nextLong = parseFloat(getLatOrLongFromString(latLongArray,(a + 1),1));
                var thisLong = parseFloat(getLatOrLongFromString(latLongArray,a,1));
                var diffInLong = Math.abs(nextLong - thisLong);
                var difInLongForEveryMin = diffInLong / minsArray[a];

                for (var b = 0; b <numMins - 1; b++)
                {
                    count++;
                    if(count == driveTime)
                    {
                        if(thisLat < nextLat)
                        {
                            nextMinLat = thisLat + difInLatForEveryMin;
                            document.getElementById('lat').value = nextMinLat;
                        }
                        else
                        {
                            nextMinLat = thisLat - difInLatForEveryMin;
                            document.getElementById('lat').value = nextMinLat;
                        }
                        if(thisLong < nextLong)
                        {
                            nextMinLong = thisLong + difInLongForEveryMin;
                            document.getElementById('lng').value = nextMinLong;
                        }
                        else
                        {
                            nextMinLong = thisLong - difInLongForEveryMin;
                            document.getElementById('lng').value = nextMinLong;
                        }
                    }
                }
            }
        }
    });
}

function findPlaces() {

    // prepare variables (filter)
    var type = document.getElementById('gmap_type').value;
    var radius = document.getElementById('gmap_radius').value;
    var keyword = document.getElementById('gmap_keyword').value;

    var lat = document.getElementById('lat').value;
    var lng = document.getElementById('lng').value;
    var cur_location = new google.maps.LatLng(lat, lng);

    // prepare request to Places
    var request = {
        location: cur_location,
        radius: radius,
        types: [type]
    };
    if (keyword) {
        request.keyword = [keyword];
    }

  //  var divA= document.getElementById('a');
    //divA.innerHTML = '';
    // send request
    service = new google.maps.places.PlacesService(map);
    service.search(request, createMarkers);

}

function createMarkers(results, status) {

    /*
     if (status == google.maps.places.PlacesServiceStatus.OK) {

     clearOverlays();

     var service = new google.maps.places.PlacesService(map);
     var request = {
     reference: place.reference
     };



     // and create new markers by search result
     for (var i = 0, place; place = results[i]; i++)  {

     service.getDetails(request, function(place, status) {
     createMarker(results[i]);
     document.getElementById('test').value += "" + place.name + ',';
     document.getElementById('test1').value += "" + results[i].types + ',';
     document.getElementById('test2').value += "" + place.formatted_phone_number + ',';
     })
     }


     }


     */



    if (status == google.maps.places.PlacesServiceStatus.OK) {
        // if we have found something - clear map (overlays)
        clearOverlays();

        // and create new markers by search result
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
            // document.getElementById('test').value += "" + results[i].name + ',';
            //  document.getElementById('test1').value += "" + results[i].types + ',';
            //   document.getElementById('test2').value += "" + results[i].postal_code + ',';
        }
    } else if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
        alert('Sorry, nothing is found');
    }
}

/**
 *
 **/
function getLatOrLongFromString(array,iterator,index)
{
    //Get the latude and longitude string
    var latAndLongString = array[iterator] + "";
    //Remove the brackets from the string
    var result = latAndLongString.substring(1, latAndLongString.length-1);
    //Split the string into seperate words
    var latOrLong = result.split(",");
    return latOrLong[index];
}

function getStringInSeconds(str) {
    //var str = "6 hrs and 40 mins";
    var words = str.split(" ");
    //var hours = parseInt(words[0]);
    // var hoursInSeconds = hours * 60 * 60;
    // var mins = parseInt(words[2]);
    // var minsInseconds = mins * 60;
    return words[0];
}
function getMinsFromDuration(str,s) {
    var words = str.split(" ");

    if (words.length <= 2) {

        return words[0];
    }
    else {
        var hoursToMins = parseInt(words[0]) * 60;
        var mins = parseInt(words[2]);
        return hoursToMins + mins;
    }
}

// clear overlays function
function clearOverlays() {
    if (markers) {
        for (i in markers) {
            markers[i].setMap(null);
        }
        markers = [];
        infos = [];
    }
}

// clear infos function
function clearInfos() {
    if (infos) {
        for (i in infos) {
            if (infos[i].getMap()) {
                infos[i].close();
            }
        }
    }
}
/*
 // creare single marker function
 function createMarker(obj) {
 var divA;
 var divB;
 var divC;
 // prepare new Marker object
 var mark = new google.maps.Marker({
 position: obj.geometry.location,
 map: map,
 title: obj.name
 });
 markers.push(mark);

 // prepare info window
 var infowindow = new google.maps.InfoWindow({
 content: '<img src="' + obj.icon + '" /><font style="color:#000;">' + obj.name +
 '<br />Rating: ' + obj.rating + '<br />Vicinity: ' + obj.vicinity + '</font>'
 });


 // add event handler to current marker
 google.maps.event.addListener(mark, 'click', function() {
 clearInfos();
 infowindow.open(map,mark);
 });
 infos.push(infowindow);
 }*/
function createMarker(place) {
    var placeLoc = place.geometry.location;
    var infowindow = new google.maps.InfoWindow();
    var mark = new google.maps.Marker({
        map: map,
        position: place.geometry.location
    });
    markers.push(mark);

    var request = { reference: place.reference };
    service.getDetails(request, function(details, status) {
        google.maps.event.addListener(mark, 'click', function() {
            clearInfos();
            infowindow.setContent(details.name + "<br />" + details.formatted_address +"<br />" + details.website + "<br />" + details.rating + "<br />" + details.formatted_phone_number);
            infowindow.open(map, this);
        });
        infos.push(infowindow);
        document.getElementById('test').value += "" + details.name + ',';
        document.getElementById('test1').value += "" + details.formatted_address + ',';
        document.getElementById('test2').value += "" + details.formatted_phone_number + ',';

    });
}