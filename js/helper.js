/**
 * Created by John on 30/04/2017.
 */

/**
 * pop-up box that appears when the user clicks on Help icon
 */
$(document).ready(function()
{
    $('[data-toggle="help"]').popover();
});