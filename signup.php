<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 31/03/2017
 * Time: 11:54
 */


/*
 * Database connection
 */
require('include/register.php');

?>

<!--
Reference
http://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TruckSmart - Parking Registration</title>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/map.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <!-- Fav icons-->
    <link rel="apple-touch-icon" sizes="57x57" href="/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo.png">
    <meta name="theme-color" content="#ffffff">


</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="formStyle">

            <div class="img-responsive" style="">
                <img src="logo.png" style="max-width: 40%;min-width: 25%;display:block;margin:auto">
            </div>

            <div class="container">
                <i class="fa fa-question-circle fa-2x" data-toggle="help"
                   style="float: right; margin-top: -50px; margin-left: 5px; " title="Registration"
                   data-content="Sign up by providing a valid email address and password. .Are you already new member? Simply click on 'back' to return to the login screen"></i>
            </div>
            <form action="include/register.php" method="post">
                <div style="margin-bottom: 25px; width: 80%;" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input id="email" type="text" class="form-control" name="email" value=""
                           placeholder="email">
                </div>
                <div style="margin-bottom: 25px; width: 80%;" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password"
                           placeholder="password" value="">
                </div>
                <div style=" display:block; margin:auto; " class="form-group">
                    <div class="col-sm-12 controls">
                        <div class="buttonSection">
                            <input type="submit" id="submit" value="Register" class="btn btn-success"
                                   style="background-color: #327772; color: #ffffff "/>
                            <span class="formChange">

                        <p id="help">Return to Login <a href="signin.php">Back</a></p>
                    </span>
                        </div>
                    </div>
            </form>

            <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <!-- Helper popup-->
            <script src="js/helper.js"></script>
        </div>
    </div>
</div>
</body>
</html>
