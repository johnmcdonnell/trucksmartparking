/**
 * Created by John on 29/03/2017.
 */

/**
 * Converts a given number of minutes in seconds
 * @param numMinutes the number of minutes to convert to seconds
 * @returns {number}
 */
function convertMinutesToMS(numMinutes)
{
    return numMinutes * 60 * 1000;
}

/**
 * Converts a given number of hours to seconds
 * @param numHours the number of hours to seconds
 * @returns {number}
 */
function convertHoursToMS(numHours)
{
    return numHours * 60 * 60 * 1000;
}

/**
 * Converts a given number of hours and minutes to seconds
 * @param numHours
 * @param numMinutes
 * @returns {number}
 */
function convertHoursAndMinsToMS(numHours,numMinutes)
{
    var hoursInMS = convertHoursToMS(numHours);
    var minInMS = convertMinutesToMS(numMinutes);

    return hoursInMS + minInMS;
}

/**
 * Convert ms into a readable format of hrs:mins:secs
 * @param ms the number of milliseconds to make readable
 * @returns {string}
 */
function convertMSToReadableFormat(ms){
    var hours = ms / (1000 * 60 * 60);
    var absHours = Math.floor(hours);
    var readableHours = absHours > 9 ? absHours : '0' + absHours;

    var minutes = (hours - absHours) * 60;
    var absMinutes = Math.floor(minutes);
    var readableMinutes = absMinutes > 9 ? absMinutes : '0' +  absMinutes;

    var seconds = (minutes - absMinutes) * 60;
    var absSeconds = Math.floor(seconds);
    var readableSeconds = absSeconds > 9 ? absSeconds : '0' + absSeconds;

    return readableHours + ':' + readableMinutes+ ':' + readableSeconds;
}

function convertMSToMinsAndSeconds(ms)
{
    var seconds = Math.floor(ms / 1000);
    var minutes = Math.floor(seconds / 60);
    seconds = seconds - (minutes * 60);
    return minutes + ':' + seconds;
}


/**
 * Gets todays date
 * @returns {number}
 */
function getDate()
{
    var date = new Date();
    return date.getDate();
}

/**
 * Gets the month we are currently in
 * @returns {number}
 */
function  getMonth()
{
    var date = new Date();
    return date.getMonth() + 1;
}

/**
 * Gets the year we are currently in
 * @returns {number}
 */
function getYear()
{
    var date = new Date();
    return date.getFullYear()
}

/**
 * Displays the current time formated fashion(e.g 14:01)
 * @param element the div to display the time in
 * @param date date object
 * @param text verbiage to display with the time (e.g. Start Time)
 * @param boolean 0 displays time in ms, anything else displays it as a time
 */
function displayCurrentTime(element,text,boolean)
{
    var date = new Date();
    var hrs = date.getHours();
    var mins = date.getMinutes();
    var readableHours = hrs > 9 ? hrs : '0' + hrs;
    var readableMinutes = mins > 9 ? mins : '0' +  mins;

    if(boolean == 0)
    {
        document.getElementById(element).innerHTML +=  text  + date.getTime() +  "<br />";
    }
    else
    {
        document.getElementById(element).innerHTML +=  text + readableHours+ ':' + readableMinutes + "<br />";
    }

}
