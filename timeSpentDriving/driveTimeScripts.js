/**
 * Created by John on 25/03/2017.
 */


var twentyfourHoursAsMS = convertHoursToMS(24);
/**
 * Possible lengths of time a driver can work in one day
 */

var fifteenHourWorkDaysAsMS = convertHoursToMS(15);
var tenHoursDailyDriveTimeAsMS = convertHoursToMS(10);
var nineHoursDailyDriveTimeAsMS = convertHoursToMS(9);

var maxDriveTimeOneSession = convertHoursAndMinsToMS(4, 30);
var breakTime = convertMinutesToMS(45);

var TAB = "&emsp;";
var canDrive = false;
var timeAfterFirstBreak = 0;


function calculateDriveTimeForOneDay()
{
    /**
     *    Check if it is the first time you are entering the site today
     *    isNewDay = true; //using this to test for first part of the day DELETE LATER
     */

    var aBool = checkIfNewDay();
    if (aBool)
    {
        displayTextInDiv('cals', 'Is a new day <br />');
        var date = new Date();
        var startTimeInMS = date.getTime();
        localStorage.setItem('startTime', startTimeInMS);

        displayCurrentTime('cals', 'Start Time in MS:', 0);
        displayCurrentTime('cals', 'Start Time: ', 1);

        var driveTime = localStorage.getItem('driveTime');

        /**
         * driveTime converted to ms
         */
        driveTime = driveTime * 60 * 1000;


        canDrive = true;

        /**
         * take away the length of time a driver has worked today so far
         */
        displayTextInDiv('hltw', 'Time left to work in a day');

        fifteenHourWorkDaysAsMS = fifteenHourWorkDaysAsMS - driveTime;
        localStorage.setItem('WD15', fifteenHourWorkDaysAsMS);
        displayTextInDiv('hltw', 'Time left in a 15hr day = ' + convertMSToReadableFormat(fifteenHourWorkDaysAsMS));

        tenHoursDailyDriveTimeAsMS = tenHoursDailyDriveTimeAsMS - driveTime;
        localStorage.setItem('WD10', tenHoursDailyDriveTimeAsMS);
        displayTextInDiv('hltw', 'Time left in a 10hr day = ' + convertMSToReadableFormat(tenHoursDailyDriveTimeAsMS));

        nineHoursDailyDriveTimeAsMS = nineHoursDailyDriveTimeAsMS - driveTime;
        localStorage.setItem('WD9', nineHoursDailyDriveTimeAsMS);
        displayTextInDiv('hltw', 'Time left in a 9hr day = ' + convertMSToReadableFormat(nineHoursDailyDriveTimeAsMS));

        /**
         * Move the time of the day on
         */
        var timeOfDayItShouldBe = startTimeInMS + driveTime;

        displayTextInDiv('cals', 'Time after driving first stint  = ' + new Date(timeOfDayItShouldBe) + '<br />');

        /**
         *  Add the break time
         */
        timeAfterFirstBreak = timeOfDayItShouldBe + breakTime;
        localStorage.setItem('timeAfterFirstBreak', timeAfterFirstBreak);
        displayTextInDiv('cals', 'Time after first break  = ' + new Date(timeAfterFirstBreak));

    }
    /**
     * Second driving stint
     */
    else
    {

        displayTextInDiv('cals', 'Is NOT a new day');
        var timeAfterFirstBreak2 = parseInt(localStorage.getItem('timeAfterFirstBreak'));
        displayTextInDiv('cals', 'Time after first stop with break taken  = ' + new Date(timeAfterFirstBreak2));
        displayTextInDiv('cals', 'Time after first stop in ms with break taken = ' + localStorage.getItem('timeAfterFirstBreak'));
        var d = new Date();

        /**
         * Get the time he is going to start driving at again and store it(localstorage)
         */
        displayTextInDiv('cals', 'Current time in ms  = ' + d.getTime());


        var startSecondStintInMs = d.getTime();
        localStorage.setItem('secondstart', startSecondStintInMs);

        startSecondStintInMs = startSecondStintInMs + driveTime;

        /**
         * check if the current time is more than the length of time he has driven plus his break
         */

        if (startSecondStintInMs < timeAfterFirstBreak2)
        {
            canDrive = false;
            alert('You cannot drive for another ' + convertMSToMinsAndSeconds(timeAfterFirstBreak2 - startSecondStintInMs) + ' minutes')
        }
        else
        {
            canDrive = true;

            /**
             * If he can drive do the same calculations as the start of day
             */
            driveTime = localStorage.getItem('driveTime');
            /**
             * driveTime converted to ms
             * @type {number}
             */
            driveTime = driveTime * 60 * 1000;

            /**
             * take away the length of time a driver has worked today so far
             */
            displayTextInDiv('hltw', 'Time left to work in a day');
            fifteenHourWorkDaysAsMS = localStorage.getItem('WD15');
            fifteenHourWorkDaysAsMS = fifteenHourWorkDaysAsMS - driveTime;
            displayTextInDiv('hltw', 'Time left in a 15hr day = ' + convertMSToReadableFormat(fifteenHourWorkDaysAsMS));

            tenHoursDailyDriveTimeAsMS = localStorage.getItem('WD10');
            tenHoursDailyDriveTimeAsMS = tenHoursDailyDriveTimeAsMS - driveTime;
            displayTextInDiv('hltw', 'Time left in a 10hr day = ' + convertMSToReadableFormat(tenHoursDailyDriveTimeAsMS));

            nineHoursDailyDriveTimeAsMS = localStorage.getItem('WD9');
            nineHoursDailyDriveTimeAsMS = nineHoursDailyDriveTimeAsMS - driveTime;
            displayTextInDiv('hltw', 'Time left in a 9hr day = ' + convertMSToReadableFormat(nineHoursDailyDriveTimeAsMS));

        }

    }

}

/**
 * Display's gieven text in a given div
 * @param div the div to display the text in
 * @param text the text to display in the div
 */
function displayTextInDiv(div, text)
{
    document.getElementById(div).innerHTML += text + " <br />"
}


/**
 * Checks if the date set in localstorage when the driveTime is set is the same as the current date
 * @returns {boolean} true if it is the same day, false if it is not the same day
 */
function checkIfNewDay()
{

    if (localStorage.getItem("oldDate") === null)
    {
        localStorage.setItem('oldDate', getDate());
        localStorage.setItem('oldMonth', getMonth());
        localStorage.setItem('oldYear', getYear());
        return true;
    }
    else
    {
        var oldDate = parseInt(localStorage.getItem('oldDate'));
        var oldMonth = parseInt(localStorage.getItem('oldMonth'));
        var oldYear = parseInt(localStorage.getItem('oldYear'));
        var newDay = getDate();
        var newMonth = getMonth();
        var newYear = getYear();
        return oldDate < newDay || oldMonth < newMonth || oldYear < newYear;
    }
}

/**
 * Gets the current date in a local string format
 * @returns {string}
 */
function getCurrentDate()
{
    var dateStr = new Date().toLocaleString().slice(0, 10);
    return dateStr.substring(0, dateStr.length - 1);
}

/**
 * Gets the current time in ms from the 1st of January 1970
 * @returns {number}
 */
function getCurrentTimeInMS()
{
    return new Date().getTime();
}

/**
 * this populates the possible places to park  table
 */
function populateTable()
{
    var array = localStorage.getItem('array').split(',');
    var array1 = localStorage.getItem('array1').split(',');
    var array2 = localStorage.getItem('array2').split(',');
    var tbl = "<table class='table table-striped' align='center' id='mytable'>";

    var colheader = '<tr><th>Name</th><th>Address</th><th>Phone Number</th><th>Facilities</th>';

    tbl += colheader;
    for (var i = 0; i < array.length - 1; i++)
    {
        tbl += "<tr><td>" + array[i] + "</td>";
        tbl += "<td>" + array1[i] + "</td>";
        tbl += "<td>" + array2[i] + "</td>";
        tbl += "<td><i class='fa fa-shower'><i></i><i class='fa fa-bed'></i><i class='fa fa-cutlery'></i><i class='fa fa-dollar'</i><i class='fa fa-truck'</i></td>";
        tbl += "</tr>";
    }
    tbl += "</table>";

    this.document.write(tbl);
}

/**
 https://www.w3schools.com/bootstrap/bootstrap_modal.asp
 */
function parkingSelected()
{
    /**
     *  Get the modal
     */

    var modal = document.getElementById('myModal');

    /**
     *  Get the button that opens the modal
     */
    var btn = document.getElementById("mytable");

    /**
     * Get the <span> element that closes the modal
     */
    var span = document.getElementsByClassName("close")[0];

    /**
     * When the user clicks the button, open the modal
     */
    btn.onclick = function ()
    {
        modal.style.display = "block";
    }

    /**
     * When the user clicks on <span> (x), close the modal
     */
    span.onclick = function ()
    {
        modal.style.display = "none";
    }

    /**
     * When the user clicks anywhere outside of the modal, close it
     * @param event
     */
    window.onclick = function (event)
    {
        if (event.target == modal)
        {
            modal.style.display = "none";
        }
    }
}

/**
 * Adds a click handler to the possible places to park table
 */
function addRowHandlers(tableID)
{

    /**
     * Reference:  https://www.youtube.com/watch?v=pUQFToFrMF4&t=15s
     */

    var table = document.getElementById(tableID);

    var rows = table.getElementsByTagName("tr");
    for (var i = 0; i < rows.length; i++)
    {
        var currentRow = table.rows[i];
        var createClickHandler =
            function (row)
            {
                return function ()
                {
                    var nameCell = row.getElementsByTagName("td")[0];
                    var parkingName = nameCell.innerHTML;
                    var addressCell = row.getElementsByTagName("td")[1];
                    var address = addressCell.innerHTML;
                    var phoneCell = row.getElementsByTagName("td")[2];
                    var phoneNumber = phoneCell.innerHTML;
                    /**
                     * Local storage
                     */
                    localStorage.setItem('pkName', parkingName);
                    localStorage.setItem('pkAdd', address);
                    localStorage.setItem('pkPhone', phoneNumber);

                    parkingSelected();
                    /**
                     * open the test page
                     */
                    window.open('algorithmBreakDown.php');

                };
            };
        currentRow.onclick = createClickHandler(currentRow);

    }
}

/**
 * Break down Daily Algorithm.
 */
displayTextInDiv('cals', 'Variable test');
displayTextInDiv('time', 'Seconds in 24 hours =' + TAB + TAB + TAB + TAB + TAB + TAB + TAB + twentyfourHoursAsMS);
displayTextInDiv('time', 'Seconds in 15 hours =' + TAB + TAB + TAB + TAB + TAB + TAB + TAB + fifteenHourWorkDaysAsMS);
displayTextInDiv('time', 'Seconds in 10 hours =' + TAB + TAB + TAB + TAB + TAB + TAB + TAB + tenHoursDailyDriveTimeAsMS);
displayTextInDiv('time', 'Seconds in 9 hours  = ' + TAB + TAB + TAB + TAB + TAB + TAB + TAB + nineHoursDailyDriveTimeAsMS);
displayTextInDiv('time', 'Seconds in 4 hours Thirty minutes =' + TAB + maxDriveTimeOneSession);
displayTextInDiv('time', 'Today\'s date  =' + TAB + getCurrentDate());
displayTextInDiv('time', 'Current time in seconds  =' + TAB + new Date(getCurrentTimeInMS()));


calculateDriveTimeForOneDay();

