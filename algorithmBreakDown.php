<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 29/03/2017
 * Time: 22:56
 */
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Test Driver Times</title>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        #time
        {
            margin-top: 30px;
            background-color: #3c3c3c;
            color: #ffffff;
        }
        #cals
        {
            margin-top: 30px;
            background-color: purple;
            color: #ffffff;
        }
        #latLong
        {
            margin-top: 30px;
            background-color: #2b542c;
            color: #ffffff;
        }
        #hltw
        {
            margin-top: 30px;
            background-color: #2b669a;
            color: #ffffff;
        }
    </style>
</head>
<body >
<div class="container-fluid">
    <div class="row">

    
<?php
include ('template/menu.php')
?>

<div id="time"></div>

<div id="cals"></div>

<div id = "hltw"></div>//Hours left that he/she can work

<div id="latLong"></div>


<?php
include ('template/footer.php')
?>

</div>
</div>
<script src="./timeSpentDriving/driverTimeCals.js"></script>
<script src="./timeSpentDriving/driveTimeScripts.js"></script>

</body>
</html>
