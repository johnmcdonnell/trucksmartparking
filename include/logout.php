<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 31/03/2017
 * Time: 00:55
 */

/*
 * References
 *
 * https://moodle.dkit.ie/201617/mod/url/view.php?id=126732
 * http://forums.devshed.com/php-faqs-stickies-167/program-basic-secure-login-system-using-php-mysql-891201.html
 *
 */

// First we execute our common code to connection to the database and start the session
require("common.php");

// We remove the user's data from the session
unset($_SESSION['user']);

// We redirect them to the login page
header("Location: index.php");
die("Redirecting to: index.php");