
<!--
 * Created by PhpStorm.
 * User: John
 * Date: 01/02/2017
 * Time: 20:08
 -->

<!-- Reference: http://codepen.io/bootstrapped/pen/EVqrdN -->

<!-- navbar -->

<div class="menu">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="img-responsive" src="logoText.png" align="left" alt="TruckSmart - Parking" width="200" height="350">

            </div>
            <div id="navbar3" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li class="dropdown">
                        <a href="/include/logout.php" style="color: #ffffff"; class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Logout</a>
                    </li>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </nav>
</div>