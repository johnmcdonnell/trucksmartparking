<!--
 * Created by PhpStorm.
 * User: John
 * Date: 12/02/2017
 * Time: 17:04
 -->

<!-- database connection -->
<?php
require_once('include/login.php');
?>

<!--
Reference
http://bootsnipp.com/snippets/featured/login-amp-signup-forms-in-panel
-->
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/map.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="apple-touch-icon" sizes="57x57" href="/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo.png">
    <meta name="theme-color" content="#ffffff">

    <title>TruckSmart - Parking Sign In</title>

</head>
<body>
<div class="container-fluid">
    <div class="row">

        <div class="img-responsive" style="">
            <img src="logo.png" style="max-width: 40%;min-width: 25%;display:block;margin:auto">
        </div>
        <div class="container">
            <i class="fa fa-question-circle fa-2x" data-toggle="help"
               style="float: right; margin-top: -50px; margin-left: 5px; " title="Login"
               data-content="Login in using your email and password.Are you a new member? Simply click on 'Create new account'"></i>
        </div>

        <form action="include/login.php" method="post">

            <div style="margin-bottom: 25px; width: 40%" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" name="email" value="<?php echo $submitted_email; ?>" class="form-control"
                       placeholder="joebloggs@gmail.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"/>
            </div>

            <div style="margin-bottom: 25px;width: 40%" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" name="password" value="" class="form-control"
                       placeholder="password - Eg. Ab123456" required pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"/>
            </div>

            <div style=" display:block; margin:auto; " class="form-group">
                <div class="col-sm-12 controls">
                    <div class="buttonSection">
                        <input type="submit" name="submit" value="Login" class="btn
                           btn-success" style="background-color: #327772; color: #ffffff; margin: auto"/>

                        <span class="formChange">
               <a style="color: #ffffff;"> <span style="font-size:1.0em;"
                                                 class="glyphicon glyphicon-question-sign"></span></a>
                                    <p>New User? <a href="signup.php"> Create new account</a></p>

                                </span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


</body>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>

<!-- popup helper -->
<script src="js/helper.js"></script>
</html>