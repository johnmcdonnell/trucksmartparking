<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 24/03/2017
 * Time: 17:20
 */

/*
 * Database connection
 */
require_once('include/common.php');
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>TruckSmart - Parking Map</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/map.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="js/createComponentsUtility.js"></script>
    <script src="js/mapScript.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Favicon for different platforms-->
    <link rel="apple-touch-icon" sizes="57x57" href="/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo.png">
    <meta name="theme-color" content="#ffffff">


</head>
<body>
<div class="container-fluid">
    <div class="row">
        <!-- include menu -->
        <?php
        include 'template/menu.php'
        ?>

        <!-- panel containing all options for map-->
        <div id="floating-panel">

            <div id="topWrapper">
                <b>Start:</b>

                <!-- dropdown for selecting journey starting point-->
                <select id="start">
                    <option value="Avonmouth, UK">Avonmouth</option>
                    <option value="Birmingham, UK">Birmingham</option>
                    <option value="Chester, UK">Chester</option>
                    <option value="Dundee, UK">Dundee</option>
                    <option value="Edinburgh,UK">Edinburgh</option>
                    <option value="Glasgow, UK">Glasgow</option>
                    <option value="Heysham, UK">Heysham</option>
                    <option value="Holyhead, United Kingdom">Holyhead</option>
                    <option value="Liverpool, UK">Liverpool</option>
                    <option value="London, UK">London</option>
                    <option value="Manchester, UK">Manchester</option>
                    <option value="Newport, UK">Newport</option>
                    <option value="Plymouth, UK">Plymouth</option>

                </select>

                <!-- dropdown for selecting waypoints-->
                <b>Waypoint:</b>

                <select id="waypoints">
                    <option value="Avonmouth, UK">Avonmouth</option>
                    <option value="Birmingham, UK">Birmingham</option>
                    <option value="Chester, UK">Chester</option>
                    <option value="Dundee, UK">Dundee</option>
                    <option value="Edinburgh,UK">Edinburgh</option>
                    <option value="Glasgow, UK">Glasgow</option>
                    <option value="Heysham, UK">Heysham</option>
                    <option value="Holyhead, United Kingdom">Holyhead</option>
                    <option value="Liverpool, UK">Liverpool</option>
                    <option value="London, UK">London</option>
                    <option value="Manchester, UK">Manchester</option>
                    <option value="Newport, UK">Newport</option>
                    <option value="Plymouth, UK">Plymouth</option>

                </select>

                <!-- dropdown for selecting waypoints-->
                <b>Destination:</b>
                <select id="end">
                    <option value="Avonmouth, UK">Avonmouth</option>
                    <option value="Birmingham, UK">Birmingham</option>
                    <option value="Chester, UK">Chester</option>
                    <option value="Dundee, UK">Dundee</option>
                    <option value="Edinburgh,UK">Edinburgh</option>
                    <option value="Glasgow, UK">Glasgow</option>
                    <option value="Heysham, UK">Heysham</option>
                    <option value="Holyhead, United Kingdom">Holyhead</option>
                    <option value="Liverpool, UK">Liverpool</option>
                    <option value="London, UK">London</option>
                    <option value="Manchester, UK">Manchester</option>
                    <option value="Newport, UK">Newport</option>
                    <option value="Plymouth, UK">Plymouth</option>
                </select>

                <!-- sets places api to 'parking locations'-->
                <div class="button" hidden>
                    <label for="gmap_type">Type:</label>
                    <select id="gmap_type">
                        <option value="parking">parking</option>


                    </select>
                </div>

                <!-- sets radius for parking locations-->
                <label for="gmap_radius">Radius:</label>
                <select id="gmap_radius">
                    <option value="5000">5000</option>
                    <option value="6000">6000</option>
                    <option value="7000">7000</option>
                    <option value="8000">8000</option>
                    <option value="9000">9000</option>
                    <option value="10000">10000</option>

                </select>

                <!-- starting location for places api'-->
                <div hidden>
                    <input type="text" id="lat" name="lat" value="53.787522"/>
                    <input type="text" id="lng" name="lng" value="-1.5918407000000343"/>
                </div>

                <div hidden>
                    <label for="gmap_where">Where:</label>
                    <input id="gmap_where" type="text" name="gmap_where"/>
                </div>

                <div id="button2" class="button" hidden onclick="findAddress(); return false;"></div>

                <div class="button">
                    <label for="gmap_keyword" hidden>Keyword (optional):</label>
                    <input id="gmap_keyword" hidden type="text" name="gmap_keyword"/>
                </div>

                <div id="directions-panel" hidden></div>


                <!-- holding selected dropdown values -->
                <input id="test" hidden type="text" name="test"/>
                <input id="test1" hidden type="text" name="test1"/>
                <input id="test2" hidden type="text" name="test2"/>


                <!-- help popup-->
                <div class="container">
                    <i class="fa fa-question-circle fa-2x" data-toggle="help" style="float: right"
                       title="Plan a Route and Find Parking"
                       data-content="Select a starting point, Select a Waypoint and Select an End point. Next Select a radius and click 'Plan Route', Next click 'Find Parking'"></i>
                </div>

                <div id="buttons">
                    <input type="submit" class="btn
                           btn-success" value="Plan Route" id="submit" style="background-color: #327772;">

                    <input type="button" class="btn
                           btn-success" value="Find Parking " onclick="postData()" style="background-color: #327772;"/>


                </div>


            </div>

        </div>
    </div>

    <!-- Map is displayed in this div -->
    <div id="map"></div>
    <div class="row">

        <?php
        include 'template/footer.php'
        ?>
    </div>
</div>
<script>


    /*
     Storing selected values in local storage
     */
    function postData()
    {

        var val = document.getElementById("test").value;
        var val1 = document.getElementById("test1").value;
        var val2 = document.getElementById("test2").value;
        localStorage.setItem("array", val);
        localStorage.setItem("array1", val1);
        localStorage.setItem("array2", val2);
        window.location.href = 'parking.php';
        document.getElementById("test").value = "";
        document.getElementById("test1").value = "";
        document.getElementById("test2").value = "";
    }
</script>
<!-- Help popup function and API KEY -->
<script src="js/helper.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpJay7na9UDs47XBlvNgeV6onWXswXQN4&libraries=places&callback=initMap">
</script>

</body>
</html>