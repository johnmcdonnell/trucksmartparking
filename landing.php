<!--
 * Created by PhpStorm.
 * User: John
 * Date: 01/02/2017
 * Time: 20:11
-->

<!-- Database connection -->
<?php
require_once('include/common.php');
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <script src="js/time.js"></script>
    <script src="js/createComponentsUtility.js"></script>

    <!-- Fav Icons for difference platforms -->
    <link rel="apple-touch-icon" sizes="57x57" href="/logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/logo.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo.png">
    <meta name="theme-color" content="#ffffff">


    <title>TruckSmart - Parking Landing</title>
</head>
<body>
<div class="container-fluid">
<div class="row">
    <!-- include menu -->
    <?php
    include 'template/menu.php'
    ?>

    <div class="container">

        <form>
            <div class="form-group">
                <h4>Please enter the vehicle registration and remaining driving hours.</h4>

                <input id="vehicleReg" type="text" class="form-control" placeholder="Enter vehicle registration"
                       style="width: 80%; margin: auto;"><br>

                <!-- Help pop up -->
                <div class="container">
                    <i class="fa fa-question-circle fa-2x" data-toggle="help"
                       style="float: right; margin-top: -50px; margin-left: 5px; "
                       title="Vehicle Registration and Working Hours"
                       data-content="Enter the vehicle registration. Next enter your remaining working hours."></i>
                </div>

                <!-- Timer dropdown - hrs -->
                <div class="input-group">
                    <label for="selectHrs"><b>Hours: </b></label>

                    <!-- Sending selected time to localstorage -->
                    <select id="selectHrs" onchange="setDrivetimeValueInLocalStorage()">
                        <option value="optionHrs">Select hours To drive</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>

                    </select>


                    <!-- Timer dropdown - mins -->

                    <label for="selectMins"><b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Minutes: </b></label>

                    <!-- Sending selected time to localstorage -->
                    <select id="selectMins" onchange="setDrivetimeValueInLocalStorage()">
                        <option value="optionMins">Select Minutes To drive</option>
                        <option value="0">0</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="25">25</option>
                        <option value="30">30</option>
                        <option value="35">35</option>
                        <option value="40">40</option>
                        <option value="45">45</option>
                        <option value="50">50</option>
                        <option value="55">55</option>
                    </select>
                </div>
                <br><br>


                <input type="button" id="btnGetLocation" type="submit" value="Plan Journey" class="btn btn-default"
                       onClick="document.location.href='map.php'"/>
            </div>
        </form>


    </div>

    <!-- include footer -->
    <?php
    include 'template/footer.php'
    ?>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

<!-- Function for pop up helper -->
<script src="js/helper.js"></script>
</body>
</html>