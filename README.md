# TruckSmart - Parking README #

## How do I get set up? ##

* XAMPP is required.
* All development was done on PhpStorm 2016.3 by JetBrains.
* Pull the repo and place it inside the 'htdocs' folder within XAMPP.
* Load the SQL database provided in the project submission.
* Open the project within an IDE and click run.

###Please note all API's have a daily quota. ###


##### Steps to running the project:  ####
* (1) Register to use the application via the registration page.
* (2) Sign in to the application
* (3) Enter a vehicle registration
* (3.1) Select working hours and minutes.
* (3.2) Click 'plan journey'.

* (4) Select starting point.
* (4.1) Select waypoint.
* (4.2) Select desination.
* (4.3) Select radius (optional)
* (4.4) Click 'Plan Route'.
* (4.4) Click 'Find Parking'.

* (5) Click on any parking location listed.